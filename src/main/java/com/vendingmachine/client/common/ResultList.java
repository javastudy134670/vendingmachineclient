package com.vendingmachine.client.common;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import static com.vendingmachine.client.App.gson;

public record ResultList<T>(String code, String msg, List<T> dataList) {
    public static<T> ResultList<T> parseJson(String json, Class<T> classOfT){
        JsonObject jsonObject = gson.fromJson(json, JsonObject.class);
        JsonArray dataElm = jsonObject.getAsJsonArray("data");
        JsonElement msgElm = jsonObject.get("msg");
        List<T> dataList = new ArrayList<>();
        if (dataElm != null) {
            for (JsonElement jsonElement : dataElm) {
                dataList.add(gson.fromJson(jsonElement, classOfT));
            }
        }
        String msg = null;
        if (msgElm != null) {
            msg = msgElm.getAsString();
        }
        return new ResultList<>(jsonObject.get("code").getAsString(), msg, dataList);
    }
}
