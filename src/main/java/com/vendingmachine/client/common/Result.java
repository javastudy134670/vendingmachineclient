package com.vendingmachine.client.common;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import static com.vendingmachine.client.App.gson;

public record Result<T>(String code, String msg, T data) {
    public static final String CODE_200 = "200";//success

    public static <T> Result<T> parseJson(String json, Class<T> classOfT) {
        JsonObject jsonObject = gson.fromJson(json, JsonObject.class);
        JsonElement dataElm = jsonObject.get("data");
        JsonElement msgElm = jsonObject.get("msg");
        T data = null;
        if (dataElm != null) {
            data = gson.fromJson(dataElm, classOfT);
        }
        String msg = null;
        if (msgElm != null) {
            msg = msgElm.getAsString();
        }
        return new Result<>(jsonObject.get("code").getAsString(), msg, data);
    }

    public static Result<Object> parseJson(String json) {
        JsonObject jsonObject = gson.fromJson(json, JsonObject.class);
        JsonElement msgElm = jsonObject.get("msg");
        String msg = null;
        if (msgElm != null) {
            msg = msgElm.toString();
        }
        return new Result<>(jsonObject.get("code").getAsString(), msg, null);
    }
}
