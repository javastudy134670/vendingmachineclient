package com.vendingmachine.client;

import com.vendingmachine.client.common.Result;
import com.vendingmachine.client.common.ResultList;
import com.vendingmachine.client.entity.Inventory;
import com.vendingmachine.client.entity.Machine;
import com.vendingmachine.client.entity.User;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class HttpOperations {
    private static final OkHttpClient client = new OkHttpClient();
    private static final String httpRoot = "http://localhost:8080";

    public static Result<Object> register(String user_name, String phone) {
        FormBody formBody = new FormBody.Builder()
                .add("user_name", user_name)
                .add("phone", phone)
                .build();
        return Result.parseJson(post(formBody, "/user/register"));
    }


    public static Result<User> login(String user_name) {
        FormBody formBody = new FormBody.Builder()
                .add("user_name", user_name)
                .build();
        return Result.parseJson(post(formBody, "/client/login"), User.class);
    }

    public static Result<Object> buy(String user_name, int machine_id, String product_name, int amount) {
        FormBody formBody = new FormBody.Builder()
                .add("user_name", user_name)
                .add("machine_id", String.valueOf(machine_id))
                .add("product_name", product_name)
                .add("amount", String.valueOf(amount))
                .build();
        return Result.parseJson(post(formBody, "/client/buy"));
    }

    public static Result<Object> recharge(String user_name, double amount) {
        FormBody formBody = new FormBody.Builder()
                .add("user_name", user_name)
                .add("amount", String.valueOf(amount))
                .build();
        return Result.parseJson(post(formBody, "/client/recharge"));
    }

    public static ResultList<Machine> getMachineList() {
        return ResultList.parseJson(get("/client/machine-list"), Machine.class);
    }

    public static ResultList<Inventory> getProductList(int machine_id) {
        return ResultList.parseJson(get("/client/product-list?machine_id=" + machine_id), Inventory.class);
    }

    public static Result<Double> getPrice(String product_name) {
        return Result.parseJson(get("/client/price?product_name=" + product_name), Double.class);
    }

    private static String post(FormBody formBody, String path) {
        Request request = new Request.Builder()
                .url(httpRoot + path)
                .post(formBody)
                .build();
        String result = "";
        try (Response response = client.newCall(request).execute()) {
            assert response.body() != null;
            result = response.body().string();
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        return result;
    }

    private static String get(String path) {
        Request request = new Request.Builder()
                .url(httpRoot + path)
                .build();
        String result = "";
        try (Response response = client.newCall(request).execute()) {
            assert response.body() != null;
            result = response.body().string();
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        return result;
    }
}
