package com.vendingmachine.client;

import com.google.gson.Gson;
import com.vendingmachine.client.entity.Machine;
import com.vendingmachine.client.entity.User;
import com.vendingmachine.client.gui.MainFrame;

import javax.swing.*;
import java.util.logging.Logger;

public class App {
    public static final Gson gson = new Gson();
    public static final Logger logger = Logger.getLogger("app");
    public static User currentUser = new User("", "", 0, false);
    public static Machine selectedMachine = new Machine(-1, "", false);

    public static void main(String[] args) {
        MainFrame mainFrame = MainFrame.getInstance();
        mainFrame.setVisible(true);
        logger.info("The program was successfully launched.");
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}