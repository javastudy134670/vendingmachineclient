package com.vendingmachine.client.entity;

public record Inventory(int machine_id, String product_name, int amount) {
}
