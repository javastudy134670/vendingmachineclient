package com.vendingmachine.client.entity;

public record Commodity(String product_name, double price, int amount) {
}
