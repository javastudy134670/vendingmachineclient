package com.vendingmachine.client.entity;

public record Product(String product_name, double price, boolean enable) {
}
