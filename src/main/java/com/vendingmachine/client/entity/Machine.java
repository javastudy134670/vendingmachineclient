package com.vendingmachine.client.entity;

public record Machine(
        int id,
        String city,
        boolean enable
) {
}
