package com.vendingmachine.client.gui;


import com.vendingmachine.client.App;
import com.vendingmachine.client.HttpOperations;
import com.vendingmachine.client.common.Result;

import javax.swing.*;
import java.awt.*;

public class RechargeButton extends JButton {
    private RechargeButton() {
        super("Recharge!");
        setBackground(Color.cyan);
        setForeground(Color.white);
        setFont(new Font(Font.DIALOG, Font.BOLD, 20));
        addActionListener(e -> perform());
    }

    private void perform() {
        String user_name = App.currentUser.user_name();
        if (user_name.isBlank()) {
            JOptionPane.showMessageDialog(null, "请先登录", "错误提示", JOptionPane.ERROR_MESSAGE);
            return;
        }
        JTextField amountField = new JTextField(10);
        Object[] message = {
                "金额：", amountField
        };
        int option = JOptionPane.showConfirmDialog(null, message, "充值", JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION) {
            if (amountField.getText().isBlank()) {
                JOptionPane.showMessageDialog(null, "充值金额不能为空", "错误提示", JOptionPane.ERROR_MESSAGE);
                return;
            }
            double amount;
            try {
                amount = Double.parseDouble(amountField.getText());
                if (amount <= 0) {
                    JOptionPane.showMessageDialog(null, "充值金额必须是正数，单位是“元”", "错误提示", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "充值金额必须是正数，单位是“元”", "错误提示", JOptionPane.ERROR_MESSAGE);
                return;
            }
            Result<Object> result = HttpOperations.recharge(user_name, amount);
            if (result.code().equals(Result.CODE_200)) {
                App.logger.info("user %s recharge %.2f success".formatted(user_name, amount));
                JOptionPane.showMessageDialog(null,"充值成功","成功提示",JOptionPane.INFORMATION_MESSAGE);
                UserInfoPanel.getInstance().refresh(user_name);
            } else {
                JOptionPane.showMessageDialog(null, "失败 msg=" + result.msg(),"错误提示",JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private static final RechargeButton rechargeButton = new RechargeButton();

    public static RechargeButton getInstance() {
        return rechargeButton;
    }
}
