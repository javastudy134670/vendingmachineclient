package com.vendingmachine.client.gui;

import com.vendingmachine.client.App;
import com.vendingmachine.client.HttpOperations;
import com.vendingmachine.client.common.Result;
import com.vendingmachine.client.entity.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class UserInfoPanel extends JPanel {
    private final JLabel user_name_text = new JLabel("Username: not login yet");
    private final JLabel phone_text = new JLabel("phone: not login yet");

    private UserInfoPanel() {
        JButton switchUser = new JButton("switch user");
        JButton register = new JButton("sign up");
        switchUser.addActionListener(this::loginPerformed);
        register.addActionListener(this::registerPerformed);
        switchUser.setBackground(Color.white);
        register.setBackground(Color.blue);
        register.setForeground(Color.white);
        setBackground(Color.gray);
        add(user_name_text);
        add(phone_text);
        add(switchUser);
        add(register);
        setPreferredSize(new Dimension(150, 120));
    }

    private void loginPerformed(ActionEvent event) {
        String input = JOptionPane.showInputDialog(MainFrame.getInstance(), "用户名", "登录页面", JOptionPane.QUESTION_MESSAGE);
        if (input != null && !input.isBlank()) {
            Result<User> result = refresh(input);
            if (result.code().equals(Result.CODE_200)) {
                JOptionPane.showMessageDialog(null, "登录成功", "成功提示", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "登录失败\n" + result, "错误提示", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void registerPerformed(ActionEvent event) {
    }

    public Result<User> refresh(String user_name) {
        Result<User> result = HttpOperations.login(user_name);
        if (result.code().equals(Result.CODE_200)) {
            User user = result.data();
            user_name_text.setText("Username: " + user.user_name());
            phone_text.setText("phone: " + user.phone());
            App.currentUser = user;
            InfoPanel.getInstance().revalidateText();
            revalidate();
        }
        return result;
    }

    private static final UserInfoPanel userInfoPanel = new UserInfoPanel();

    public static UserInfoPanel getInstance() {
        return userInfoPanel;
    }
}
