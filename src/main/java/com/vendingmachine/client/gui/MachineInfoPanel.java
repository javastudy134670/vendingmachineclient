package com.vendingmachine.client.gui;

import com.vendingmachine.client.App;
import com.vendingmachine.client.HttpOperations;
import com.vendingmachine.client.common.Result;
import com.vendingmachine.client.common.ResultList;
import com.vendingmachine.client.entity.Machine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class MachineInfoPanel extends JPanel {
    private final JLabel machine_id_label = new JLabel("machine id: not selected");
    private final JLabel city_label = new JLabel("machine city: not selected");

    private static final DefaultListModel<Machine> model = new DefaultListModel<>();
    private static final JList<Machine> jList = new JList<>(model);

    static {
        jList.setBackground(Color.gray);
        jList.setCellRenderer((list1, value, index, isSelected, cellHasFocus) -> {
            JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            JLabel idLabel = new JLabel("id: " + value.id());
            JLabel cityLabel = new JLabel("city: " + value.city());
            idLabel.setForeground(Color.cyan);
            cityLabel.setForeground(Color.green);
            panel.setBackground(Color.darkGray);
            panel.setBorder(BorderFactory.createLineBorder(Color.lightGray));
            panel.add(idLabel);
            panel.add(cityLabel);
            if (isSelected) panel.setBackground(Color.gray);
            return panel;
        });
    }

    private MachineInfoPanel() {
        JButton selectMachineButton = new JButton("select machine");
        selectMachineButton.addActionListener(this::btnPerformed);
        selectMachineButton.setBackground(Color.white);
        setBackground(Color.gray);
        add(machine_id_label);
        add(city_label);
        add(selectMachineButton);
        setPreferredSize(new Dimension(150, 120));
    }

    private void btnPerformed(ActionEvent e) {
        ResultList<Machine> machineResultList = HttpOperations.getMachineList();
        if (!machineResultList.code().equals(Result.CODE_200)) {
            JOptionPane.showMessageDialog(null, "发生错误\n msg=" + machineResultList.msg());
        }
        JScrollPane scrollPane = new JScrollPane(jList);
        scrollPane.setPreferredSize(new Dimension(300, 250));
        scrollPane.getVerticalScrollBar().setUI(new MyBasicScrollBarUI());
        model.clear();
        model.addAll(machineResultList.dataList());
        int option = JOptionPane.showConfirmDialog(null, scrollPane, "选择当前机器", JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION) {
            Machine machine = jList.getSelectedValue();
            machine_id_label.setText("machine id: " + machine.id());
            city_label.setText("machine city: " + machine.city());
            App.selectedMachine = machine;
            CommodityViewPanel.getInstance().reFresh();
        }
    }

    private static final MachineInfoPanel dimeCoinPanel = new MachineInfoPanel();

    public static MachineInfoPanel getInstance() {
        return dimeCoinPanel;
    }
}
