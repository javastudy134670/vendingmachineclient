package com.vendingmachine.client.gui;

import com.vendingmachine.client.App;

import javax.swing.*;
import java.awt.*;

public class InfoPanel extends JPanel {
    private final JLabel overview = new JLabel();

    private InfoPanel() {
        JPanel topPanel = new JPanel();
        JPanel bottomPanel = new JPanel();
        JPanel gap = new JPanel();
        gap.setBackground(Color.gray);
        overview.setForeground(Color.white);
        topPanel.setBackground(Color.darkGray);
        setBackground(Color.gray);
        topPanel.add(overview);
        bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.X_AXIS));
        bottomPanel.add(UserInfoPanel.getInstance());
        bottomPanel.add(gap);
        bottomPanel.add(MachineInfoPanel.getInstance());
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        add(topPanel);
        add(bottomPanel);
        revalidateText();
    }

    public void revalidateText() {
        overview.setText("Balance: ￥%.2f".formatted(App.currentUser.balance()));
        revalidate();
    }

    private static final InfoPanel INFO_PANEL = new InfoPanel();

    public static InfoPanel getInstance() {
        return INFO_PANEL;
    }
}
