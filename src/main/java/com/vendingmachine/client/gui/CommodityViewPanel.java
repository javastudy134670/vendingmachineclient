package com.vendingmachine.client.gui;

import com.vendingmachine.client.App;
import com.vendingmachine.client.HttpOperations;
import com.vendingmachine.client.common.Result;
import com.vendingmachine.client.common.ResultList;
import com.vendingmachine.client.entity.Commodity;
import com.vendingmachine.client.entity.Inventory;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class CommodityViewPanel extends JPanel {

    public void reFresh() {
        int machine_id = App.selectedMachine.id();
        List<Commodity> commodities = new ArrayList<>();
        if (machine_id > 0) {
            ResultList<Inventory> resultList = HttpOperations.getProductList(machine_id);
            for (Inventory inventory : resultList.dataList()) {
                Result<Double> priceResult = HttpOperations.getPrice(inventory.product_name());
                commodities.add(new Commodity(inventory.product_name(), priceResult.data(), inventory.amount()));
            }
        }
        model.clear();
        model.addAll(commodities);
        revalidate();
    }

    private CommodityViewPanel() {
        JList<Commodity> list = new JList<>(model);
        JScrollPane scrollPane = new JScrollPane(list);
        TitledBorder titledBorder = new TitledBorder("PRODUCT PRICE LIST");
        JPanel options = new JPanel();
        JButton buyButton = new JButton("Buy");
        list.setBackground(Color.gray);
        scrollPane.setBackground(Color.gray);
        buyButton.setBackground(Color.green);
        buyButton.setForeground(Color.white);
        options.setBackground(Color.gray);
        buyButton.addActionListener(e -> buyPerformed(list.getSelectedValue()));
        titledBorder.setTitleJustification(TitledBorder.CENTER);
        scrollPane.setPreferredSize(new Dimension(300, 250));
        scrollPane.getVerticalScrollBar().setUI(new MyBasicScrollBarUI());
        list.setCellRenderer((list1, value, index, isSelected, cellHasFocus) -> {
            JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            JLabel nameLabel = new JLabel(value.product_name());
            JLabel priceLabel = new JLabel("￥%.2f".formatted(value.price()));
            JLabel amountLabel = new JLabel("stock left: %d".formatted(value.amount()));
            nameLabel.setForeground(Color.cyan);
            priceLabel.setForeground(Color.green);
            amountLabel.setForeground(Color.white);
            panel.setBackground(Color.darkGray);
            panel.setBorder(BorderFactory.createLineBorder(Color.lightGray));
            panel.add(nameLabel);
            panel.add(priceLabel);
            panel.add(amountLabel);
            if (isSelected) panel.setBackground(Color.gray);
            return panel;
        });
        this.setBorder(titledBorder);
        this.setLayout(new BorderLayout());
        this.add(scrollPane, BorderLayout.NORTH);
        this.add(options, BorderLayout.SOUTH);
        this.setBackground(Color.gray);
        options.add(buyButton, BorderLayout.CENTER);
        reFresh();
    }

    private void buyPerformed(Commodity commodity) {
        if (commodity == null) {
            JOptionPane.showMessageDialog(null, "还未选择产品", "错误提示", JOptionPane.ERROR_MESSAGE);
            return;
        }
        String user_name = App.currentUser.user_name();
        if (user_name.isBlank()) {
            JOptionPane.showMessageDialog(null, "请先登录", "错误提示", JOptionPane.ERROR_MESSAGE);
            return;
        }
        JTextField amountField = new JTextField(10);
        amountField.setText("1");
        Object[] message = {
                "购买数量",
                amountField
        };
        int option = JOptionPane.showConfirmDialog(null, message, "购买%s".formatted(commodity.product_name()), JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION) {
            try {
                int amount = Integer.parseInt(amountField.getText());
                if (amount > commodity.amount()) {
                    JOptionPane.showMessageDialog(null, "库存不足", "错误提示", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                if (amount < 0) {
                    JOptionPane.showMessageDialog(null, "请输入正整数", "错误提示", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                Result<Object> result = HttpOperations.buy(user_name, App.selectedMachine.id(), commodity.product_name(), amount);
                if (!result.code().equals(Result.CODE_200)) {
                    JOptionPane.showMessageDialog(null, "发生错误\n" + result, "错误提示", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, "购买成功！", "成功提示", JOptionPane.INFORMATION_MESSAGE);
                    reFresh();
                    UserInfoPanel.getInstance().refresh(user_name);
                }
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "请输入正整数", "错误提示", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private static final DefaultListModel<Commodity> model = new DefaultListModel<>();

    private static final CommodityViewPanel commodityViewPanel = new CommodityViewPanel();

    public static CommodityViewPanel getInstance() {
        return commodityViewPanel;
    }

}
