package com.vendingmachine.client.gui;

import javax.swing.*;
import java.awt.*;

public class ContentPanel extends JPanel {
    private ContentPanel() {
        setLayout(new BorderLayout());
        add(InfoPanel.getInstance(), BorderLayout.NORTH);
        add(RechargeButton.getInstance(),BorderLayout.CENTER);
        add(CommodityViewPanel.getInstance(), BorderLayout.SOUTH);
    }

    private static final ContentPanel contentPanel = new ContentPanel();

    public static ContentPanel getInstance() {
        return contentPanel;
    }

}
